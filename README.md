# Feladat - BankAccount
Készíts egy PHP osztályt BankAccount néven.
Létrehozáskor lehesen megadni az egyenleget(balance) és a pénznemet(currency)
A Következő metódusoknak kell lennie:

setCurrency(currency) - beállítja, hogy HUF vagy EUR a devizanem. Az árfolyam fixen 300 HUF = 1 EUR

getBalance() - Visszaadja az egyeneleget a beállított pénznemmel együtt szövegesen

setBalance(newBalance) - Beállítja az egyenleget. A megadott új egyenleg a korábban beállított pénznemben értendő

modifyBalance(difference) - módosítja az egyeneleget a megadott értékkel. A megadott új egyenleg a korábban beállított pénznemben értendő

A teszt.php futtatásának eredménye a kövekező kell, hogy legyen:

$ba->getBalance(): 100 EUR
$ba->getBalance(): 30000 HUF
$ba->getBalance(): 60000 HUF
$ba->getBalance(): 200 EUR
$ba->getBalance(): 120000 HUF
$ba->getBalance(): 120001 HUF
